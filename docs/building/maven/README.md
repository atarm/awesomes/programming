# Maven

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [What](#what)
    1. [Maven是什么](#maven是什么)
    2. [When & Where & Who - 谁在什么时候什么场景发明了Maven](#when--where--who---谁在什么时候什么场景发明了maven)
    3. [Maven能做什么](#maven能做什么)
    4. [Maven的核心理念/设计哲学](#maven的核心理念设计哲学)
2. [Why - 为什么需要Maven](#why---为什么需要maven)
    1. [`maven` VS `ant`](#maven-vs-ant)
    2. [When & Where & Who - 谁应该在什么时候什么场景使用Maven](#when--where--who---谁应该在什么时候什么场景使用maven)
3. [How - 如何学习和使用Maven](#how---如何学习和使用maven)
    1. [`maven`使用过程中障碍及其解决方法](#maven使用过程中障碍及其解决方法)
4. [Futhermore](#futhermore)
    1. [Books and Monographs](#books-and-monographs)
    2. [Courses and Tutorials](#courses-and-tutorials)
    3. [Documents and Supports](#documents-and-supports)
    4. [Manuals and CheatSheets](#manuals-and-cheatsheets)
    5. [Papers and Articles](#papers-and-articles)
    6. [Playgrounds and Exercises](#playgrounds-and-exercises)
    7. [Examples and Templates](#examples-and-templates)
    8. [Standards and Specifications](#standards-and-specifications)
    9. [Softwares and Tools](#softwares-and-tools)
        1. [Artifacts](#artifacts)
        2. [plugins](#plugins)
        3. [extensions](#extensions)
    10. [Miscellaneous](#miscellaneous)
    11. [Tutorials and Articles](#tutorials-and-articles)

<!-- /code_chunk_output -->

## What

### Maven是什么

`Maven` means "Accumulator of Knowledge", 中文翻译可以是"专家"或"行家"（口语）等，是`Apache`组织的一个顶级项目，声明式的、用于`Java`项目的信息管理、依赖管理和构建管理。

>`Maven` is a build automation tool used **primarily for Java projects**. Maven can also be used to build and manage projects written in C#, Ruby, Scala, and other languages. The Maven project is hosted by the **Apache Software Foundation**, where it was formerly part of the **`Jakarta` Project**.
>
>`Maven` addresses two aspects of building software: how software is **built** and its **dependencies**.
>
>Unlike earlier tools like Apache Ant, it uses conventions for the build procedure. Only exceptions need to be specified.
>
>An XML file describes the software project being built, its dependencies on other external modules and components, the build order, directories, and required plug-ins. It comes with pre-defined targets for performing certain well-defined tasks such as compilation of code and its packaging. Maven dynamically downloads Java libraries and Maven plug-ins from one or more repositories such as the Maven 2 Central Repository, and stores them in a local cache.
>
>Maven is built using a plugin-based architecture that allows it to make use of any application controllable through standard input.
>
>Alternative technologies like Gradle and sbt as build tools do not rely on XML, but keep the key concepts Maven introduced.
>
>>[wiki. Apache Maven.](https://en.wikipedia.org/wiki/Apache_Maven)

### When & Where & Who - 谁在什么时候什么场景发明了Maven

1. 2002: Jason van Zyl began `maven` as a sub-project of `Apache Turbine`
1. 2003: `maven` was voted on and accepted as a top level `Apache Software Foundation` project
1. 2004-07: `maven 1.0`
1. 2005-10: `maven 2.0`
1. 2010-10: `maven 3.0`

>[wiki. Apache Maven.](https://en.wikipedia.org/wiki/Apache_Maven)

### Maven能做什么

以功能模块划分：

1. 项目信息管理information management
    1. conventions项目默认约定：项目目录结构、测试用例命名
    1. static information项目静态信息：项目描述、开发者列表、版本控制系统地址、软件许可证、缺陷管理系统地址等
    1. dynamic reports项目动态报告：测试报告、版本日志报告、项目站点等
1. 项目依赖管理dependency management
    1. libraries dependency类库依赖
    1. plugins dependency插件依赖
1. 项目构建管理build management：`Maven`抽象并设计了一套完整的构建生命周期模型及与之配套的插件模型，这套模型也标准化了构建流程
    1. building workflow构建流程
    1. artifacts publish工件分发

### Maven的核心理念/设计哲学

1. 抽象并且统一：如，生命周期将所有的构建过程进行抽象和统一
    1. 构建流程抽象复用：`Maven`定义了一个标准的、完整的构建生命周期模型，使用插件`plugin`执行实际的构建（而不是自定义构建脚本）
    1. `pom.xml`定义了关于项目配置的所需的信息
1. 约定优于配置：
    1. 约定的目录结构
    1. 约定的构建行为
1. 坐标定位体系：`Maven`使用`groupId`，`artifactId`和`version`唯一定位一个构件

<!--
1. `Maven`中声明一个依赖项可以自动下载并导入`classpath`
1. `Maven`使用插件
-->

>Using Maven, the user provides only configuration for the project, while the configurable plug-ins do the actual work of compiling the project, cleaning target directories, running unit tests, generating API documentation and so on.
>
>>[wiki. Apache Maven.](https://en.wikipedia.org/wiki/Apache_Maven)

## Why - 为什么需要Maven

the problem:

1. `IDE`的固有缺陷
    1. 个性化配置：很难在项目中统一团队所有人的`IDE`配置，容易导致"A机器成功、B机器失败"的现象
    1. 手工操作多：编译、测试、打包等工作相互独立，很难一键完成所有工作，依赖大量的手工操作
    1. 脱离`IDE`仍需构建：
1. `Ant`的固有缺陷
    1. 手动管理依赖：没有内置的依赖管理功能（可以借助`Ivy`管理依赖）
    1. 构建脚本复杂：基于命令式的构建流程定义，每个项目均需定义构建规则和流程

Maven的优势：

1. easy build process
1. central repositories for dependencies
1. universal build system
1. make new features easy❓

### `maven` VS `ant`

>The fundamental difference between `Maven` and `Ant` is that `Maven`'s design regards **all projects** as having **a certain structure** and **a set of supported task work-flows** (e.g., getting resources from source control, compiling the project, unit testing, etc.). While most software projects in effect support these operations and actually do have a well-defined structure, Maven requires that this structure and the operation implementation details be defined in the `POM` file. Thus, Maven relies on a convention on how to define projects and on the list of work-flows that are generally supported in all projects.
>>[wiki. Apache Maven.](https://en.wikipedia.org/wiki/Apache_Maven)

### When & Where & Who - 谁应该在什么时候什么场景使用Maven

## How - 如何学习和使用Maven

### `maven`使用过程中障碍及其解决方法

1. `IDE`支持度
    - situation：`IDE`的`Maven plugin`可能导致`Maven`版本及配置不一致、不稳定等情况
    - solution：尽可能通过`cli`使用`Maven`，将`IDE`的`Maven plugin`作为辅助工具（如：查看`Maven`配置）
1. 项目依赖之间的兼容性
    - situation：工件之间、插件之间、工件与插件之间可能出现版本兼容性问题
    - solution：项目依赖之间的兼容性不是依赖管理工具需要解决的问题（依赖管理工具解决依赖定位和依赖版本解析），而是项目本身要解决的问题
1. `Maven`配置复杂度高

## Futhermore

### Books and Monographs

<!-- {contents here} -->

1. 👍 [许晓斌. Maven实战[M]. 机械工业出版社. 2014-07.](https://book.douban.com/subject/5345682/)

### Courses and Tutorials

<!-- {contents here} -->

1. 🏛️ [Maven Users Centre](https://maven.apache.org/users/index.html)
    1. [Maven in 5 Minutes](https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html)
1. 🌟 [廖雪峰.Java教程-Maven基础[DB/OL]](https://www.liaoxuefeng.com/wiki/1252599548343744/1255945359327200)
1. 👍 [Maven on MacOS (OSX)](https://wilsonmar.github.io/maven-on-macos/)
1. 🌟 [hackjutsu/maven-in-action](https://github.com/hackjutsu/maven-in-action)

### Documents and Supports

<!-- {contents here} -->

1. 🏛️ [Official Maven Documentation](https://maven.apache.org/guides/index.html)
    1. [Lifecycles Reference](https://maven.apache.org/ref/current/maven-core/lifecycles.html)
    1. [apache.org. Maven CLI Options Reference.](https://maven.apache.org/ref/3-LATEST/maven-embedder/cli.html): the same as `mvn -h`

### Manuals and CheatSheets

<!-- {contents here} -->

### Papers and Articles

<!-- {contents here} -->

### Playgrounds and Exercises

<!-- {contents here} -->

### Examples and Templates

<!-- {contents here} -->

1. [jacoco/jacoco-maven-plugin.test/](https://github.com/jacoco/jacoco/tree/master/jacoco-maven-plugin.test)
1. [Maven Usage Example](https://www.eclemma.org/jacoco/trunk/doc/examples/build/pom.xml)
1. [Maven Usage Offline Example](https://www.eclemma.org/jacoco/trunk/doc/index.html)

### Standards and Specifications

<!-- {contents here} -->

### Softwares and Tools

<!-- {contents here} -->

#### Artifacts

1. 👍 [MVN Repository.](https://mvnrepository.com/)
1. [Sonatype](https://repository.sonatype.org/)
    1. [Sonatype. Maven Repositories](https://help.sonatype.com/repomanager3/nexus-repository-administration/formats/maven-repositories)
    1. [Sonatype. The Central Repository Documentation](https://central.sonatype.org/)
1. 🌟 Maven Central Repository
    1. [Maven Central Repository Search](https://search.maven.org)
    1. [Maven Central Repository](https://repo.maven.apache.org/maven2)
1. 🌟 AliYun Maven Repository
    1. [阿里云云效Maven指南](https://maven.aliyun.com/mvn/guide)
    1. [阿里云云效Maven搜索](https://maven.aliyun.com/mvn/search)
1. [Google's Maven Repository](https://maven.google.com/web/index.html)

#### plugins

1. 🏛️ [apache.org. Apache Maven Plugins](https://maven.apache.org/plugins/index.html)(include some common used plugins in `mojohaus` and misc)
1. [MojoHaus Maven Plugins](https://www.mojohaus.org/plugins.html)

#### extensions

1. 🏛️ [apache.org. Maven Extensions](https://maven.apache.org/extensions/index.html)

### Miscellaneous

<!-- {contents here} -->

### Tutorials and Articles
