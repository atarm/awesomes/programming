# Best Practices最佳实践

## Convention Over Configuration约定优于配置

### Conventions of Directory Structure目录结构约定

>1. [Apache. Introduction to the Standard Directory Layout.](https://maven.apache.org/guides/introduction/introduction-to-the-standard-directory-layout.html)

```sh {.line-numbers}
${basedir}
|-- pom.xml
|-- src
|    |-- main
|    |    `-- java
|    |    `-- resources
|    |    `-- filters
|    `-- test
|    |    `-- java
|    |    `-- resources
|    |    `-- filters
|    `-- it
|    `-- assembly
|    `-- site
|-- target
`-- LICENSE
`-- NOTICE
`-- README
```

<https://jerrychin.github.io/2020/04/27/alibaba-pmd-checking.html>

|Directory name|Purpose|
|:--|:--|
project home|Contains the pom.xml and all subdirectories.
`src/main/java`|Contains the deliverable Java sourcecode for the project.
`src/main/resources`|Contains the deliverable resources for the project, such as property files.
`src/test/java`|Contains the testing Java sourcecode (JUnit or TestNG test cases, for example) for the project.
`src/test/resources`|Contains resources necessary for testing.

### Conventions of Naming命名约定

1. `groupId`：`corporation.project-group-name`
1. `artifactId`: `project-name`
1. `package`: `${groupId}.${artifactId-like}.xxx`
    - `${groupId}`: 建议与`groupId`一致
    - `${artifactId-like}`: 建议包含或体现`artifactId`，如，将`artifactId`去除`-`形成一个包或包层级

多模块命名：

1. 各子模块使用相同的`groupId`
1. 各子模块的`artifactId`使用相同的前缀
1. 各子模块的`dirname`使用`artifactId`（删除前缀并替换连字符为下划线后的`artifactId`）
1. 一起开发和发布的子模块使用相同的`version`
