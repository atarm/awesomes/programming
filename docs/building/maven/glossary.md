# Glossary

## Sonatype

## `POM`

`POM(Project Object Model)`。

>A Project Object Model (POM) provides all the configuration for a single project.
>>[wiki. Apache Maven.](https://en.wikipedia.org/wiki/Apache_Maven)

## `plugin`

<!--TODO: add-->
>A plugin provides a set of goals that can be executed using the command `mvn [plugin-name]:[goal-name]`.
>>[wiki. Apache Maven.](https://en.wikipedia.org/wiki/Apache_Maven)

## `goal`

>Goals provided by plugins can be associated with different phases of the lifecycle.
>>[wiki. Apache Maven.](https://en.wikipedia.org/wiki/Apache_Maven)

## `mojo`

`mojo(Maven plain Old Java Object/Maven Ordinary Java Object)`。

一个`mojo`对应`plugin`中一个`goal`。

## `lifecycle`

>The build lifecycle is a list of named phases that can be used to give order to goal execution.
>>[wiki. Apache Maven.](https://en.wikipedia.org/wiki/Apache_Maven)
