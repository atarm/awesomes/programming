//1. 引入express
const express = require("express");

//2. 创建应用对象
const app = express();

// 使用中间件来解析 JSON 和 URL 编码的数据
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//3. 创建路由规则
// request 是对请求报文的封装
// response 是对响应报文的封装
app.get("/server", (request, response) => {
    //设置响应头  设置允许跨域
    response.setHeader("Access-Control-Allow-Origin", "*");
    //设置响应体
    response.send("HELLO AJAX - GET"+`\nYour request query is ${JSON.stringify(request.query)}`);
});

app.post("/server", (request, response) => {
    //设置响应头  设置允许跨域
    response.setHeader("Access-Control-Allow-Origin", "*");
    //设置响应头：允许自定义请求头
    response.setHeader("Access-Control-Allow-Headers", "*");
    console.log(request.headers);
    //设置响应体
    response.send("HELLO AJAX - POST" + `\nYour request body is ${JSON.stringify(request.body)}`);
});

//可以接收任意类型的请求方法
app.all("/server", (request, response) => {
    //设置响应头  设置允许跨域
    response.setHeader("Access-Control-Allow-Origin", "*");
    //响应头
    response.setHeader("Access-Control-Allow-Headers", "*");
    //设置响应体
    response.send("HELLO AJAX - ALL");
});

//JSON 响应
app.all("/json-server", (request, response) => {
    //设置响应头  设置允许跨域
    response.setHeader("Access-Control-Allow-Origin", "*");
    //响应头
    response.setHeader("Access-Control-Allow-Headers", "*");
    //响应一个数据
    const data = {
        name: "json-server",
    };
    //设置响应体
    response.send(JSON.stringify(data));
});

//针对 IE 缓存
app.get("/ie", (request, response) => {
    //设置响应头  设置允许跨域
    response.setHeader("Access-Control-Allow-Origin", "*");
    //设置响应体
    response.send("HELLO AJAX - IE");
});

//延时响应
app.all("/delay", (request, response) => {
    //设置响应头  设置允许跨域
    response.setHeader("Access-Control-Allow-Origin", "*");
    response.setHeader("Access-Control-Allow-Headers", "*");
    setTimeout(() => {
        //设置响应体
        response.send("HELLO AJAX - DELAY");
    }, 3000);
});

//jQuery 服务
app.all('/jquery-server', (request, response) => {
    //设置响应头  设置允许跨域
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Allow-Headers', '*');
    const data = { name: 'jquery-server' };
    response.send(JSON.stringify(data));
});

//axios 服务
app.all('/axios-server', (request, response) => {
    //设置响应头  设置允许跨域
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Allow-Headers', '*');
    const data = { name: 'axios-server' };
    response.send(JSON.stringify(data));
});

//fetch 服务
app.all('/fetch-server', (request, response) => {
    //设置响应头  设置允许跨域
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Allow-Headers', '*');
    const data = { name: 'fetch-server' };
    response.send(JSON.stringify(data));
});

//jsonp服务
app.all('/jsonp-server', (request, response) => {
    const data = {
        name: 'jsonp-server'
    };
    //将数据转化为字符串
    let str = JSON.stringify(data);
    //返回结果
    response.end(`handle(${str})`);
});

//用户名检测是否存在
app.all('/check-username', (request, response) => {
    const data = {
        exist: 1,
        msg: '用户名已经存在'
    };
    //将数据转化为字符串
    let str = JSON.stringify(data);
    //返回结果
    response.end(`handle(${str})`);
});

//
app.all('/jquery-jsonp-server', (request, response) => {
    const data = {
        name: 'jquery-jsonp-server',
        city: ['北京', '上海', '深圳']
    };
    //将数据转化为字符串
    let str = JSON.stringify(data);
    //接收 callback 参数
    let cb = request.query.callback;

    //返回结果
    response.end(`${cb}(${str})`);
});

app.all('/cors-server', (request, response) => {
    //设置响应头
    response.setHeader("Access-Control-Allow-Origin", "*");
    response.setHeader("Access-Control-Allow-Headers", '*');
    response.setHeader("Access-Control-Allow-Method", '*');
    // response.setHeader("Access-Control-Allow-Origin", "http://127.0.0.1:5500");
    response.send('hello CORS');
});


// 启动服务器
app.listen(8000, () => {
    console.log("服务器已经启动，8000 端口监听中...");
});