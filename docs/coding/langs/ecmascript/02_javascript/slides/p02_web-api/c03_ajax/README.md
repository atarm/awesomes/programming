---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_AJAX_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# AJAX

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Introduction of AJAX](#introduction-of-ajax)
2. [XMLHttpRequest and Fetch](#xmlhttprequest-and-fetch)
3. [jQuery and Axios](#jquery-and-axios)
4. [Same-Origin Policy and Cross-Origin Resource Sharing](#same-origin-policy-and-cross-origin-resource-sharing)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Introduction of AJAX

---

### What is AJAX

---

1. `ajax(Asynchronous Javascript And Xml, 异步JavaScript与XML)`
2. `ajax`允许客户端与服务器异步通信，客户端可以在不重新加载整个网页的情况下，与服务器交换数据并更新部分网页内容
3. `ajax`不是新的编程语言，而是一种将现有技术组合在一起的新方法
4. `ajax`最初采用`SOAP`和`XML`技术，后来演变成使用`JSON`格式的数据

---

![height:600](./.assets/images/ajax-vergleich-en.jpg)

---

### Brief History of AJAX

1. 1996: the iframe tag was introduced by Internet Explorer
2. 1998: the Microsoft Outlook Web Access team developed the concept behind the XMLHttpRequest scripting object.It appeared as XMLHTTP in the second version of the MSXML library
3. 1999: the Microsoft XMLHTTP ActiveX Object Library was released, allowing for the first time the use of AJAX in Internet Explorer 5.0
4. 2004: Google made a wide deployment of AJAX in their Gmail(2004) and Google Maps(2005) applications
5. 2005: the term AJAX was coined by Jesse James Garrett in "Ajax: A New Approach to Web Application" published on the Adaptive Path website

---

### AJAX的优点

1. 局部刷新

---

### AJAX的缺点

1. 没有浏览历史，不能回退
2. 存在跨域问题
3. `SEO`不友好

---

### How to Use AJAX

---

1. `web-api`原生
    1. `XMLHttpRequest`
    2. `Fetch`
2. 封装库
    1. `jQuery`
    2. `axios`

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## XMLHttpRequest and Fetch

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### XMLHttpRequest

> [MDN. XMLHttpRequest API.](https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest_API)

---

```javascript {.line-numbers}
// 1. 创建一个新的 XMLHttpRequest 对象
var xhr = new XMLHttpRequest();

// 2.1. 配置请求方法和目标URL
xhr.open('GET', 'https://api.example.com/data', true);

// 2.2. 设置请求头
xhr.setRequestHeader('Content-Type', 'application/json');

// 3. 设置回调函数，处理响应
xhr.onreadystatechange = function() {
    // 4. 检查请求是否完成
    if (xhr.readyState === 4) {
        // 5. 检查响应状态码是否为200（成功）
        if (xhr.status === 200) {
            // 6. 解析响应数据
            // xhr.responseText
            // xhr.responseXML
            var data = JSON.parse(xhr.responseText);
            console.log(data);
        } else {
            console.error('请求失败，状态码：' + xhr.status);
        }
    }
};

// 发送请求
xhr.send();
```

---

#### `open()`

```javascript {.line-numbers}
open(method, url)
open(method, url, async)
open(method, url, async, user)
open(method, url, async, user, password)
```

>[MDN. XMLHttpRequest: open() method.](https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/open)

---

#### `send()`

```javascript {.line-numbers}
send()
send(body)
```

>[MDN. XMLHttpRequest: send() method.](https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/send)

---

#### `readyState`

1. `xhr.readyState`属性表示请求的当前状态
2. `xhr.readyState`是`xhr`的状态码，有5个值（0至4）

---

1. `0 (UNSENT)`: 请求还未初始化，即，`new`之后、`open()`之前
2. `1 (OPENED)`: 请求已经建立，但还未发送，即，`open()`之后、`send()`之前
3. `2 (HEADERS_RECEIVED)`: 请求已发送，头部和状态已接收，即，`send()`之后、`xhr.responseHeaders`可用
4. `3 (LOADING)`: 请求处理中，响应体部分已经接收，即，`xhr.responseText`部分数据可用
5. `4 (DONE)`: 请求完成，响应已完全接收或接收失败，即，`xhr.responseText`全部数据可用

>[MDN. XMLHttpRequest: readyState property.](https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/readyState)

---

#### Events

1. `readystatechange`/`xhr.onreadystatechange`: whenever the `readyState` property of the `XMLHttpRequest` changes
2. `load`/`xhr.onload`: when an XMLHttpRequest transaction completes successfully
3. `error`/`xhr.onerror`: when the request encountered an error
4. `timeout`/`xhr.ontimeout`: when progression is terminated due to preset time expiring

---

1. `abort`/`xhr.onabort`: when a request has been aborted
2. `loadstart`/`xhr.onloadstart`: when a request has started to load data
3. `loadend`/`xhr.onloadend`: when a request has completed, whether successfully (after `load`) or unsuccessfully (after `abort` or `error`)
4. `progress`/`xhr.onprogress`: periodically when a request receives more data

---

```javascript {.line-numbers}
const xhr = new XMLHttpRequest();

xhr.open('GET', 'https://api.example.com/data', true);

// Event listener for download progress
xhr.onprogress = function(event) {
    if (event.lengthComputable) {
        const percentComplete = (event.loaded / event.total) * 100;
        console.log(`Download progress: ${percentComplete.toFixed(2)}%`);
    } else {
        console.log(`Downloaded ${event.loaded} bytes`);
    }
};

// Event listener for successful completion
xhr.onload = function() {
    if (xhr.status >= 200 && xhr.status < 300) {
        console.log('Download complete:', xhr.responseText);
    } else {
        console.error('Download failed with status:', xhr.status);
    }
};

// Event listener for errors
xhr.onerror = function() {
    console.error('Download error');
};

// Send the request
xhr.send();
```

---

```javascript {.line-numbers}
const xhr = new XMLHttpRequest();

xhr.open('POST', 'https://api.example.com/upload', true);

// Event listener for upload progress
xhr.upload.onprogress = function(event) {
    if (event.lengthComputable) {
        const percentComplete = (event.loaded / event.total) * 100;
        console.log(`Upload progress: ${percentComplete.toFixed(2)}%`);
    } else {
        console.log(`Uploaded ${event.loaded} bytes`);
    }
};

// Event listener for successful completion
xhr.onload = function() {
    if (xhr.status >= 200 && xhr.status < 300) {
        console.log('Upload complete:', xhr.responseText);
    } else {
        console.error('Upload failed with status:', xhr.status);
    }
};

// Event listener for errors
xhr.onerror = function() {
    console.error('Upload error');
};

// Create form data to upload
const formData = new FormData();
formData.append('file', fileInput.files[0]);

// Send the request with the form data
xhr.send(formData);
```

---

```javascript {.line-numbers}
const xhr = new XMLHttpRequest();

xhr.open('GET', 'https://api.example.com/data', true);

// Set timeout duration (e.g., 5000 milliseconds = 5 seconds)
xhr.timeout = 5000;

// Event listener for successful completion
xhr.onload = function() {
    if (xhr.status >= 200 && xhr.status < 300) {
        console.log('Request complete:', xhr.responseText);
    } else {
        console.error('Request failed with status:', xhr.status);
    }
};

// Event listener for errors
xhr.onerror = function() {
    console.error('Request error');
};

// Event listener for timeout
xhr.ontimeout = function() {
    console.error('Request timed out');
};

// Send the request
xhr.send();
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### Fetch

>[MDN. Fetch API.](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API)

---

1. `Fetch`是更标准的、用于替代`XMLHttpRequest`的技术
2. `Fetch`返回`Promise`对象

---

```javascript {.line-numbers}
fetch(resource)
fetch(resource, options)
```

>[MDN. Window: fetch() method.](https://developer.mozilla.org/en-US/docs/Web/API/Window/fetch)

---

```javascript {.line-numbers}
fetch('https://api.example.com/data')
    .then(response => {
        if (!response.ok) {
            throw new Error('请求失败，状态码： ' + response.status);
        }
        return response.json();
    })
    .then(data => console.log(data))
    .catch(error => console.error('请求失败：', error));
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### XMLHttpRequest vs Fetch

---

1. 语法：`XMLHttpRequest`基于`callback-nested`，`Fetch`基于`Promise`
2. 功能：
    1. `Fetch`支持`Request`对象、`Response`对象、`Stream`对象等
    2. `XMLHttpRequest`支持上传下载进度事件，`Fetch`不支持
    3. `XMLHttpRequest`支持设置超时属性，`Fetch`不支持
3. 兼容：`Fetch`需`browser`至少支持`Promise`（当前主流`browser`均支持），`Fetch`不支持`IE`浏览器
4. 运行环境：`node-fetch`提供与`Fetch`类似的功能

---

![height:550](./.assets/images/image-20241023121418332.png)

><https://caniuse.com/?search=fetch>

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## jQuery and Axios

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### jQuery

<!-- >[]() -->

---

1. `.load()`：元素加载数据
2. `jQuery.ajax()`
    1. `jQuery.get()`
    2. `jQuery.post()`

---

### `.load()`

1. `.load()`：从服务器加载数据，并把返回的数据放入被选元素中
2. 通常`.load()`方法适用于发送一个`url`请求，把服务器上的一个文件、页面加载到客户端

```javascript {.line-numbers}
$(selector).load(URL, data, callback);
```

>[jquery api. `.load()`.](https://api.jquery.com/load/)

---

```html {.line-numbers}
<div id="add_Address" style="display: none">
</div>
```

```javascript {.line-numbers}
$('#add_address').load('/distpicker.html');
```

---

```javascript {.line-numbers}
if (pathname=='/'&&method=='get'){
    fs.readFile(path.join(__dirname,'views/newAddress.html'),function (err,data) {
        if (err){
            return res.end(err.message);
        }
        res.end(data);
    });
}else if (pathname=='/distpicker.html'&&method=='get'){
    //处理客户端的load( )路由请求
    fs.readFile(path.join(__dirname,'views/distpicker.html'),function (err,data) {
        if (err){
            return res.end(err.message);
        }
        res.end(data);
    });
}
```

---

### `jQuery.ajax()`

1. `ajax()`：通过`HTTP`请求加载远程数据
1. `ajax()`是`jQuery`底层`Ajax`实现，简单易用的高层实现有`$.get()`,`$.post()`等

```javascript {.line-numbers}
$.ajax([settings]);
```

>[jquery api. `jQuery.ajax().`](https://api.jquery.com/jQuery.ajax/)

---

1. `url`：请求的`URL`，默认是当前页面
1. `type`：请求方式（默认为`get`）
1. `data`：发送到服务器的数据
1. `success(result,status,xhr)`：当请求成功时运行的函数
    1. `result`由服务器返回
    1. `status`,`xhr`描述状态的字符串

---

```javascript {.line-numbers}
//保存地址
$('#save_addr').click(function () {
    //拼接地址
    let address = [$('#province').val(), $('#city').val(), $('#district').val(), $('#newAddress').val()];
    //构造ajax数据,假设用户号为1，在后面的实例中用户号由服务器转递
    $.ajax({
        url: '/insertAddr', type: 'GET', data: {
            userId: 1, receiver: $('#receiver').val(), receiverTel: $('#receiverTel').val(), address: address.toString()
        }, success: function (str) {//Ajax数据请求成功，收到响应
            //识别响应
            let json = eval("(" + str + ")");
            if (json.ok) {
                //添加成功
                alert('添加成功');
                $('#popup_win').css('display', 'none');
                $('#add_Address').css('display', 'none');
                //此处添加刷新地址列表的请求与响应代码
            }
        }, error: function (err) {//Ajax请求不成功
            console.log(err);
        }
    });
});
```

---

```javascript {.line-numbers}
else if (pathname === '/insertAddr' && method === 'get') {
    model.doInsertAddress(GET.userId, GET.receiverTel, GET.receiver, GET.address, function (err, results2) {
        if (err) {
            return res.end(err.message);
        }
        let json = {ok: true, msg: '收货地址添加成功', userID: GET.userId};
        res.end(JSON.stringify(json));
    });
}
```

---

### `jQuery.get()` and `jQuery.post()`

```javascript {.line-numbers}
$.get(url, [data], [success(res,status,xhr)], [res_datatype]);
$.post(url, [data], [success(data, status, xhr)],[res_datatype]);
```

>[jquery api. `jQuery.get()`.](https://api.jquery.com/jQuery.get/)
>[jquery api. `jQuery.post()`.](https://api.jquery.com/jQuery.post/)

---

```javascript {.line-numbers}
$.get({
    url:'/refreshAddress',
    data:{userId:json.userId},
    success:function(result) {
        //接收到来自服务器的数据信息并转化
        let data = eval("(" + result + ")");
        console.log(data)//控制台进行数据查看
        let htmlStr = '';
        $("#addresslist").empty();//清空客户端原地址列表
        for (let i = 0; i < data.length; i++) {
            htmlStr += '<label><input type="checkbox" value="' + data[i].id + '" id="' + data[i].id + '" class="checkbox_anniu">';
            htmlStr += data[i].receiver + data[i].receiver_tel + data[i].address +'</label></p>';
        }
        $("#addresslist").append( htmlStr);//添加地址列表
    }
});
```

---

```javascript {.line-numbers}
$.post("/loginAjax", {username: username, password: password}, function (str) {
    //识别响应
    let json = eval("(" + str + ")");
    if (json.ok) {
        alert('登录成功');
        console.log(json.userId);
        window.location.href = '/userCenter.html';
    } else {
        alert(json.msg);
    }
});
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### Axios

>1. [axios/axios.](https://github.com/axios/axios)
>2. [axios docs.](https://axios-http.com/docs/intro)

---

1. `axios`是基于`Promise`的`HTTP`客户端，可用于`browser`和`node.js`
2. 在`browser`中，`axios`基于`XMLHttpRequest`或`Fetch`实现
3. 在`node.js`中，`axios`基于`http`等相关模块实现

---

```javascript {.line-numbers}
axios(config)

axios(url[, config])
```

---

```javascript {.line-numbers}
// For convenience aliases have been provided for all supported request methods.

axios.request(config)
axios.get(url[, config])
axios.delete(url[, config])
axios.head(url[, config])
axios.options(url[, config])
axios.post(url[, data[, config]])
axios.put(url[, data[, config]])
axios.patch(url[, data[, config]])
axios.postForm(url[, data[, config]])
axios.putForm(url[, data[, config]])
axios.patchForm(url[, data[, config]])
```

---

```javascript {.line-numbers}
// GET
const axios = require('axios');

axios.get('https://api.example.com/data').then(response => {
        console.log(response.data);
    }).catch(error => {
        console.error('请求失败：', error);
    });
```

---

```javascript {.line-numbers}
// POST
const axios = require('axios');

axios.post('https://api.example.com/data', {
    key1: 'value1',
    key2: 'value2'
}).then(response => {
        console.log(response.data);
    }).catch(error => {
        console.error('请求失败：', error);
    });
```

---

```javascript {.line-numbers}
// set headers
const axios = require('axios');

axios.get('https://api.example.com/data', {
    headers: {
        'Authorization': 'Bearer token'
    }
}).then(response => {
        console.log(response.data);
    }).catch(error => {
        console.error('请求失败：', error);
    });
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### jQuery VS Axios

---

1. `jquery`除了`ajax`外，还包括`DOM`操作、事件处理、动画效果等，`axios`专注于`HTTP`请求
2. `axios`基于`Promise`，`jquery`基于`nested-callback`
3. `axios`支持`browser`和`node.js`，`jquery`主要用于`browser`
4. `axios`支持`interceptors`、`cancellation`，`jquery`不支持

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Same-Origin Policy and Cross-Origin Resource Sharing

>1. [阮一峰. 跨域资源共享 CORS 详解.](https://www.ruanyifeng.com/blog/2016/04/cors.html)

---

1. `Same-Origin Policy(同源策略)`：最早由`Netscape`公司引入浏览器，目的是防止`XSS(Cross-Site Scripting)`攻击
2. `SOP`要求`protocol`、`domain`、`port`都相同，否则是跨域请求
3. 解决跨域请求的方法：`JSONP(JSON with Padding)`、`CORS(Cross-Origin Resource Sharing)`、`WebSocket`、`postMessage`等

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### JSONP

---

1. `JSONP`是一种非官方的跨域解决方案
2. `JSONP`支持`GET`请求，不支持`POST`请求
3. `JSONP`的原理是利用一些具有跨域功能的`HTML`标签，比如`<img>`、`<script>`、`<iframe>`等，通过`src`属性来请求跨域资源，然后在`HTML`标签中嵌入一些`JavaScript`代码，通过`JavaScript`代码来处理跨域请求返回的数据

---

```html {.line-numbers}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JSONP Example</title>
</head>
<body>
    <h1>JSONP Example</h1>
    <div id="result"></div>

    <script>
        // callback
        function handleResponse(data) {
            document.getElementById('result').innerText = JSON.stringify(data);
        }

        // <script> tag
        var script = document.createElement('script');
        script.src = 'https://example.com/data?callback=handleResponse'; // 替换为实际的跨域请求URL
        document.body.appendChild(script);
    </script>
</body>
</html>
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### CORS

>1. [MDN. CORS.](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS)
>2. [express. cors middleware.](https://expressjs.com/en/resources/middleware/cors.html)

---

1. `CORS`是一个`W3C`标准，是一种官方的跨域解决方案
2. 工作原理：通过设置一个响应头`Access-Control-Allow-Origin`，来告诉浏览器该请求是允许跨域的，浏览器根据这个响应头来判断是否允许跨域请求
3. 需要浏览器和服务器同时支持（不需要在`browser`做特殊操作，需要`browser`支持`CORS`）
4. 服务器设置`Access-Control-Allow-Origin`头信息来允许跨域请求
5. 通信过程，由`browser`自动执行，不需要人工参与
    1. 对于开发者而言：`CORS`通信与`SOP AJAX`通信没有差别
    2. 对于最终用户而言：浏览器一旦发现`AJAX`请求跨源，就会自动添加一些附加的头信息，有时还会多出一次附加的请求，但用户不会有感觉

---

```javascript {.line-numbers}
const express = require('express');
const cors = require('cors');

const app = express();
const port = 3000;

// 使用 CORS 中间件
app.use(cors());

// 定义一个简单的 API 路由
app.get('/data', (req, res) => {
    res.json({
        message: 'Hello, this is a CORS-enabled response!',
        name: 'GitHub Copilot',
        role: 'AI Programming Assistant'
    });
});

app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
```

---

```html {.line-numbers}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CORS Example</title>
</head>
<body>
    <h1>CORS Example</h1>
    <div id="result"></div>

    <script>
        fetch('http://localhost:3000/data')
            .then(response => response.json())
            .then(data => {
                document.getElementById('result').innerText = JSON.stringify(data);
            })
            .catch(error => console.error('Error:', error));
    </script>
</body>
</html>
```

---

```bash {.line-numbers}
mkdir cors-example
cd cors-example
npm install -g nodemon
npm init -y
npm install express cors

vim server.js
vim index.html

nodemon server.js
```

<!--TODO:

---

#### 跨域传递`cookie`

```javascript {.line-numbers}
xhr.withCredentials = true      // 配置跨域传递cookie

fetch(url, {
    credentials: 'include',     // 配置跨域传递cookie
    mode: 'cors'                // 以cors的形式配置跨域
})
``` -->

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
