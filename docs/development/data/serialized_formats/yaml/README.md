# YAML(YAML Ain't Markup Language)

## What

>YAML version 1.2 is a superset of JSON; prior versions were not strictly compatible.
>>[wiki. JSON.](https://en.wikipedia.org/wiki/JSON)

>YAML ain't markup language. YAML is a concise data serialization language, which is typically considered easier to read than the more verbose JSON or XML. It is most often used in configuration files of servers and software applications. Clark Evans proposed the spec in 2001 as the result of an effort to simplify XML. According to his resume, at the time he conceived of YAML he was working with Python, which likely influenced the syntax of YAML as an indentation-based language.
>>[YAML Checker.](https://yamlchecker.com/)

## Why

### SWOT

#### Strengths

1. 人眼友好的数据分隔符：`<lf>`和`<space>`

#### Weaknesses

1. 扩展性差：
1. 格式要求：严格对齐
1. 难于压缩：难于删除语法所需的分隔符，从而难于压缩成一行（单行模式？）

#### Opportunities

#### Threats

## How

### Syntax

1. 大小写敏感
1. 缩进代表层级关系，只允许使用`<space>`，相同层级的元素左侧对齐（缩进`<space>`的数量不重要）
1. 从`#`开始到行结尾为注释
1. 四种数据结构
    1. 纯量/标量scalar：单个不可再分的值
    1. 数组array/序列sequence/列表list：一组次序相关的值，使用`-`开头的左侧对齐的行
    1. 集合set
    1. 映射mapping/对象object/字典dictionary/哈希hash：键值对表示，使用`:`分隔键和值
1. 行内表示法

### Scalar

1. 字符串`str`
1. 布尔值
1. 整数
1. 浮点数
1. Null
1. 时间
1. 日期

两个感叹号`!!`进行强制数据类型转换

## Futhermore

### Standards and Specifications

1. 🏛️ [yaml.org](https://yaml.org/)
    1. [yaml.org. YAML Ain't Markup Language (YAML&trade;) version 1.2 Revision 1.2.2. 2021-10-01.](https://yaml.org/spec/1.2.2/)
    1. [yaml.org. YAML 1.1 Reference card](https://yaml.org/refcard.html)
    1. 👍 [yaml.org. Language-Independent Types for YAML&trade; Version 1.1. 2005-01-18.](https://yaml.org/type/)

### Courses and Tutorials

1. 🌟 [阮一峰. YAML语言教程.](https://www.ruanyifeng.com/blog/2016/07/yaml.html)
1. 🌟 [阮一峰. 数据类型和Json格式.](http://www.ruanyifeng.com/blog/2009/05/data_types_and_json.html)
1. 🌟 [陈皮皮. 一文看懂 YAML.](https://zhuanlan.zhihu.com/p/145173920)
1. [Ansible. YAML Syntax.](https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html)
1. [YAML Tutorial](https://gettaurus.org/docs/YAMLTutorial/)
1. [w3schools.io. YAML - Tutorials.](https://www.w3schools.io/file/yaml-introduction/)
1. [tutorialspoint.com. YAML Tutorial.](https://www.tutorialspoint.com/yaml/index.htm)

### Papers and Articles

### Playgrounds and Exercises

### Documents and Supports

1. [wikipedia. YAML.](https://en.wikipedia.org/wiki/YAML)

### Manuals and CheatSheets

1. 👍 [quickref. YAML cheatsheet.](https://quickref.me/yaml)

### Tools

1. 👍 [YAML Demo and Parser.](https://nodeca.github.io/js-yaml/)(❗ use single quote in `JSON` ❗)
1. [Online YAML Parser.](https://yaml-online-parser.appspot.com/)(use double quote in `JSON`)
1. [YAML Checker.](https://yamlchecker.com/)
