---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_YAML Ain't Markup Language_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# YAML Ain't Markup Language

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Introduction](#introduction)
2. [Basic Syntax](#basic-syntax)
3. [Data Type](#data-type)
4. [Anchor, Reference and Merge](#anchor-reference-and-merge)
5. [Strength and Weakness](#strength-and-weakness)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Introduction

---

1. `YAML`首次发布于2001-05-11，设计的目标是方便人的读写
1. `YAML`在开发时的全称是`"Yet Another Markup Language"`，后来改为`"YAML Ain't a Markup Language"`
1. `YAML`是一种通用的串行化数据格式/流式数据格式，比`XML`和`JSON`更适合作为配置文件格式
1. `YAML 1.2.0`(2009-07-21)开始是`JSON`的超集，合法的`JSON`语法也是合法的`YAML`语法
1. `YAML`文件的扩展名为`yaml`或`yml`

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Basic Syntax

---

1. 大小写敏感
1. 使用换行符作为元素分隔符/数据项分隔符
1. 使用缩进表示层级关系（行内表示法使用`JSON`语法）
    1. 缩进时不允许使用`<tab>`，只允许使用`<space>`
    1. 缩进的空格数目不重要，要求相同层级的元素缩进的空格数目相同即可
1. `:`分元素名和元素值
    1. `:`与元素值之间必须至少有一个空格

---

1. 字符串
    1. 引号不是必须的（含`YAML`语法的特殊字符除外，如`:`）
    1. 无引号和单引号不转义不转码，双引号进行转义转码（`JSON`标准只有双引号，支持单引号的`JSON`库的单引号与双引号无区别）
    1. 支持多行字符串
1. `!!<type>`进行强制数据类型转换

---

1. `#`表示注释
    1. 从`#`到行尾是注释
    1. 只有行注释
1. 单物理文件多逻辑文件（不常用）
    1. `---`：逻辑文件的开始
    1. `...`：逻辑文件的结束（非必须）

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Data Type

---

1. `scalar`标量：`int`、`float`、`string`、`null`、`date`、`datetime`、`boolean`
    - 不常用的`scalar`：`binary`、`yaml`
1. `sequence`序列：有序数据组，`- value`
1. `mapping`映射：键值对，`key: value`
1. 实质上没有`scalar`的`set`（表示为值为`null`的`mapping`）
1. `structure`：`scalar`、`sequence`、`mapping`以及`set of mapping`的组合

> [yaml.org. Language-Independent Types for YAML&trade; Version 1.1. 2005-01-18.](https://yaml.org/type/)

---

### `scalar`

---

```yaml {.line-numbers}
i: 1                             # int
f: 1.234                         # float

s1: 'abc \n d'                   # string, "abc \\n d"
s2: "abc \n d"                   # string, "abc <lf> d"
s3: abc \n d                     # string, "abc \\n d"
s4: !!str null                   # string, "null"

n1: null                         # null
n2: ~                            # null
n3:                              # null

b1: false                        # boolean
b2: False                        # boolean
b3: FALSE                        # boolean

d: 2015-04-05                    # date
dt: 2015-04-05T02:59:43+08:00    # datetime, ISO8601 format
```

---

```yaml {.line-numbers}
# YAML
strings:
  - Hello without quote          # 不用引号
  - Hello
   world                         # 拆成多行自动在中间添加空格
  - I am fine. \u263A            # 不使用引号或使用单引号时不转码
  - "I am fine. \u263A"          # 使用双引号时支持Unicode编码
  - "\x0d\x0a is \r\n"           # 使用双引号时支持Hex编码
  - 'He said: "Hello!"' # 单双引号支持嵌套"

// JSON
{ strings: 
   [ 'Hello without quote',
     'Hello world',
     'Hello with single quotes',
     'Hello with double quotes',
     'I am fine. \\u263A',
     'I am fine. ☺',
     '\r\n is \r\n',
     'He said: "Hello!"' ] }
```

---

### `multi-line string`

---

1. 默认：删除字符串前的换行符，保留字符串内空白行的换行符，替换字符串内非空白行的一个换行符为一个空格（如果前面是空白行，则直接删除），删除对齐的缩进空格、删除额外的空格，删除字符串末尾的换行符
1. `|`保留换行符：保留字符串前的换行符，保留字符串内非空白行的换行符，删除对齐的缩进空格、保留额外的空格，保留字符串末尾的 **一个换行符**
1. `>`折叠换行符：保留字符串前的换行符，保留字符串内空白行的换行符，替换字符串内的换行符为一个空格，删除对齐的缩进空格、保留额外的空格，保留字符串末尾的 **一个换行符**

---

|字符|`default`|`"\|"`|`">"`|
|:--|--|--|--|
|串前`"\n"`|delete|keep|keep|
|串内空白行`"\n"`|keep|keep|keep|
|串内非空白行`"\n"`|`"<space>"`|keep|`"<space>"`|
|串内对齐缩进`"<space>"`|delete|delete|delete|
|串内非对齐缩进`"<space>"`|delete|keep|keep|
|串尾`"\n"`|delete|keep one|keep one|

---

```yaml {.line-numbers}
description1: 


    hello
    the
    world
description2: |


    hello
    the
    world
description3: >


    hello
    the
    world

//JSON
{ description1: 'hello the world',
  description2: '\n\nhello\nthe\nworld\n',
  description3: '\n\nhello the world\n' }
```

---

```yaml {.line-numbers}
description1: 


    hello
    
    
      the
    world
description2: |


    hello
    
    
      the
    world
description3: >


    hello
    
    
      the
    world

//JSON
{ description1: 'hello\n\nthe world',
  description2: '\n\nhello\n\n\n  the\nworld\n',
  description3: '\n\nhello\n\n\n  the\nworld\n' }
```

---

1. `+`：保留字符串末尾的所有换行符
1. `-`：删除字符串末尾的所有换行符

---

```yaml {.line-numbers}
# description1: "\nhello\nworld\nhere\n\n\n"
description1: |+

  hello
  world
  here
  

# description2: "\nhello\nworld\nhere"
description2: |-

    hello
    world
    here
    
    
```

---

### `sequence`

```yaml {.line-numbers}
- Mark McGwire
- Sammy Sosa
- Ken Griffey
```

---

### `mapping`

1. `unordered mapping`映射集合：无序的键值对集合，`set of mapping`
1. `ordered mapping`映射序列：有序的键值对集合，`sequence of mapping`

---

#### `unordered mapping`

```yaml {.line-numbers}
hr:  65       # Home runs
avg: 0.278    # Batting average
rbi: 147      # Runs Batted In
```

---

#### `ordered mapping`

```yaml {.line-numbers}
- hr:  65       # Home runs
- avg: 0.278    # Batting average
- rbi: 147      # Runs Batted In
```

---

```yaml {.line-numbers}
# Sets are represented as a
# Mapping where each key is
# associated with a null value
!!set
? Mark McGwire
? Sammy Sosa
? Ken Griffey
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Anchor, Reference and Merge

---

1. `&`：anchor/variable
1. `*`：reference
1. `<<`：merge

---

```yaml {.line-numbers}
# parent1: { a: 2, b: 3 }
parent1: &defaults
    a: 2
    b: 3

# child1: { a: 2, b: 4 }
child1:
    <<: *defaults
    b: 4

# child2: { a: 2, b: 3, c: 5 }
child2:
    <<: *defaults
    c: 5

# parent2: { d: 6 }
parent2: &another
    d: 6

# child3: { a: 2, b: 3, d: 6, e: 7 }
child3:
    <<: [*defaults, *another]
    e: 7
```

---

```yaml {.line-numbers}
# parent1: { a: 2, b: 3 }
parent1: &defaults
    a: 2
    b: 3

# child: { a: { a: 2, b: 3 }, b: 4 }
child:
    a: *defaults
    b: 4
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Strength and Weakness

---

### Strength

1. human friendly and intuitive
1. multi-line string supported
1. variable (`anchors` and `aliases`) supported

---

### Weakness

1. indentation-based
    1. would be long to write
    1. bad without editor lint supported
    1. bad without editor `<tab>` to `<space>` supported
    1. hard to `zip`(`<space>` is meaningful)

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
